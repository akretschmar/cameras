
package Cameras;


abstract class DigitalCamera {
    protected String make;
    protected String model;
    protected double megapixels;
    
public DigitalCamera(){}

    public DigitalCamera(String make, String model, double megapixels) {
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
    }
    
abstract String describeCamera();

    
}
