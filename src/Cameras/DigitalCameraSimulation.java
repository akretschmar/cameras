package Cameras;


public class DigitalCameraSimulation {

    public static void main(String[] args) {
       PointAndShootCamera pasc1 = new PointAndShootCamera("Canon", "Powershot A590", 8.0, "16GB");
       
        System.out.println(pasc1.describeCamera());
        System.out.println("----------------------");
        
        PhoneCamera pc1 = new PhoneCamera("Apple", "iPhone", 6.0, "64GB");
        System.out.println(pc1.describeCamera());
    }
    
}
