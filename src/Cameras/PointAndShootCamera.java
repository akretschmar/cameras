
package Cameras;

public class PointAndShootCamera extends DigitalCamera {
    private String externalMemorySize;
public PointAndShootCamera(){}

public PointAndShootCamera(String make, String model, double megapixels, String externalMemorySize){
    super(make, model, megapixels);
    this.externalMemorySize = externalMemorySize;
}

String describeCamera(){
    return make + "\n" + model + "\nMegapixels: " + megapixels + "\nMemory: " + externalMemorySize;
}
}
