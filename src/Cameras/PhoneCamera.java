
package Cameras;

public class PhoneCamera extends DigitalCamera {
    private String internalMemorySize;
    
    
    public PhoneCamera(){}

public PhoneCamera(String make, String model, double megapixels, String internalMemorySize){
    super(make, model, megapixels);
    this.internalMemorySize = internalMemorySize;
}

    String describeCamera(){
    return make + "\n" + model + "\nMegapixels: " + megapixels + "\nMemory: " + internalMemorySize;
}
}
